# SPDX-FileCopyrightText: 2013 - 2014 Dan Vrátil
# SPDX-FileCopyrightText: 2013 - 2016 Daniel Vrátil
# SPDX-FileCopyrightText: 2016 - 2021 Elvis Angelaccio
# SPDX-FileCopyrightText: 2016 Luigi Toscano
# SPDX-FileCopyrightText: 2017 Andreas Sturmlechner
# SPDX-FileCopyrightText: 2017 Ben Cooksley
# SPDX-FileCopyrightText: 2019 - 2022 Laurent Montel
# SPDX-FileCopyrightText: 2019 David Barchiesi
# SPDX-FileCopyrightText: 2020 - 2024 Albert Astals Cid
# SPDX-FileCopyrightText: 2020 - 2024 Nicolas Fella
# SPDX-FileCopyrightText: 2020 Christoph Feck
# SPDX-FileCopyrightText: 2020 Friedrich W. H. Kossebau
# SPDX-FileCopyrightText: 2021 Heiko Becker
# SPDX-FileCopyrightText: 2022 Ömer Fadıl Usta
# SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

cmake_minimum_required(VERSION 3.16)

set(ONEDRIVE_VERSION 0.0.5)
project(kio-onedrive VERSION ${ONEDRIVE_VERSION})

set(QT_MIN_VERSION 6.6.1)
set(KF5_MIN_VERSION 5.249.0)
set(KGAPI_MIN_VERSION 5.240.0)
set(KACCOUNTS_MIN_VERSION 17.04.0)

find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Network)

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules" ${ECM_MODULE_PATH})

include(FeatureSummary)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMQtDeclareLoggingCategory)
include(ECMSetupVersion)

find_package(KF6 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    I18n
    KIO)
find_package(KAccounts6 ${KACCOUNTS_MIN_VERSION} REQUIRED)

find_package(Qt${QT_MAJOR_VERSION}Test QUIET)
set_package_properties(Qt${QT_MAJOR_VERSION}Test PROPERTIES
    TYPE OPTIONAL
    PURPOSE "Required for building tests.")

# Until I can figure out what is wrong
#if(NOT Qt${QT_MAJOR_VERSION}Test_FOUND)
#   set(BUILD_TESTING OFF CACHE BOOL "Build the testing tree.")
#endif()

if(NOT DEFINED BUILD_TESTS)
    set(BUILD_TESTS OFF)
endif()

if(BUILD_TESTS)
    message("Will build tests")
    add_subdirectory(autotests)
endif()

ecm_setup_version(PROJECT
    VARIABLE_PREFIX ONEDRIVE
    VERSION_HEADER onedriveversion.h)

add_definitions(-DQT_NO_URL_CAST_FROM_STRING)
add_definitions(-DQT_STRICT_ITERATORS)
add_definitions(-DTRANSLATION_DOMAIN=\"kio6_onedrive\")

add_subdirectory(src)
add_subdirectory(kaccounts)
install(FILES onedrive.desktop
        DESTINATION ${KDE_INSTALL_DATADIR}/remoteview)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/configure.sh ${CMAKE_CURRENT_BINARY_DIR}/configure.sh COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/configure_llvm.sh ${CMAKE_CURRENT_BINARY_DIR}/configure_llvm.sh COPYONLY)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
