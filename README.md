<!--
SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# What this is
This is an attempt to make a KIO worker (formerly called "slave") to access OneDrive via their [public API](https://learn.microsoft.com/en-us/onedrive/developer/rest-api/), all authorization being done by [KAccounts integration](https://commits.kde.org/kaccounts-integration).

# Developing
Using LLVM is (for now) required, see [Fedora bug 2263269](https://bugzilla.redhat.com/show_bug.cgi?id=2263269).

Clang 18 or later is required (Clang 17 has a [bug](https://invent.kde.org/bernardogn/kio-onedrive/-/issues/2))

This project targets KDE 6, along with KDE Frameworks 6

## Compiling
### In a kde-builder install
In a standard kde-builder/kdesrc-build install, clone this to ~/kde/src/kio-onedrive. Create ~/kde/build/kio-onedrive, copy configure.sh there and run it, then run ninja. If someone more knowledgeable on kde-builder/kdesrc-build knows how to improve this process, you are welcome to make a suggestion or MR.

To compile with Clang (LLVM), use configure_llvm.sh instead. Using LLVM is (for now) required to compile with C++ modules

### Outside a kde-builder install
If you have the kio, kaccounts and kaccounts-integration development libraries installed, you can compile this as if it were any other CMake project: create a build directory, then run in a shell:

```
export CC=clang
export CXX=clang++
cd $BUILDDIR
cmake -B . -S $SRCDIR -G Ninja
ninja
```

If you have issues, please install the ki18n development library as well and add `-DCMAKE_CXX_COMPILER_CLANG_SCAN_DEPS:FILEPATH=/usr/bin/clang-scan-deps` as a configure time option to CMake.

Replace $BUILDDIR with your new build directory and $SRCDIR with the directory to where you cloned this repository. clang and ninja for C++ module support.

Note that setting _FORTIFY_SOURCE to any value other than "0" will cause the compilation to fail.

## Installing
To install, you may run `ninja install`, or alternatively, you may also install manually by copying these files in the build directory to their locations:

- bin/kf6/kio/onedrive.so to /usr/lib64/qt6/plugins/kf6/kio
- kaccounts/microsoft.provider to /usr/share/accounts/providers/kde
- kaccounts/microsoft-onedrive.service to /usr/share/accounts/services/kde
- onedrive.desktop (in the source folder) to /usr/share/remoteview

These locations are on Fedora, and may be different on other distros.

## Client IDs
To do the authentication necessary to access the API, an OAUTH Client ID is needed. When not specifying one, a functional, default one will be used.

To use your own, which may be necessary in enterprise settings, pass -DCLIENT_ID=yourclientidhere when configuring. It is possible to obtain a Client ID from Microsoft at [Azure](https://aka.ms/AppRegistrations/). Set the redirect URL to `http://localhost/oauth2callback` and allow the 'Files.ReadWrite.All' and 'offline_access' delegated scopes, and also set the application as a "mobile/desktop application." Either set the application as multi-tenant or pass -DTENANT=yourorgdomain when configuring. For example, if you want to set it up for an organization with domain "example.com" and your Client ID is 1fb46f92-141f-4bcc-ac79-1bf60fbfbfeb, you want to configure with `-DCLIENT_ID=1fb46f92-141f-4bcc-ac79-1bf60fbfbfeb -DTENANT=example.com`.

## Pre-built binaries
A [copr repository](https://copr.fedorainfracloud.org/coprs/bernardogn/kio-onedrive/) is avaliable for Fedora.

# Using
First, go to System Settings, Online accounts, Add new account. Then select the "Microsoft" one and log in with your account as normal. If you are having issues, try logging in with your password in a browser first, then try again. It should support two factor authentication.

Then in the Online Accounts page, click on your newly registered Microsoft account and give it a proper name with the pencil icon on the top right, and enable the Onedrive service if it is not already enabled.

Then in Dolphin, you may navigate to the "Network" tab under "Remote" on the left side of the screen, then click "OneDrive". Alternatively, you may navigate to `onedrive:/` in Dolphin.

Most operations that can be implemented are implemented. OneDrive personal, OneDrive for Business accounts are supported, Sharepoint support is tracked at [issue 5](https://invent.kde.org/bernardogn/kio-onedrive/-/issues/5).

Note that when uploading large files, the progress indicator will climb quickly, then appear to be stuck at 100%. Simply wait and the upload should finish. Run `journalctl -f -g kf6.kio.onedrive` and look for the messages "Uploading chunk of size x" to check if the operation is stuck or not. If the messages are being printed, then it is not stuck. This is because uploading is done in 2 parts, and KIO only uses the first part (which is really quick) for counting progress.
