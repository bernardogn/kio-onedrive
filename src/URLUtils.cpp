// SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

module;
#include <QUrl>
#include <QString>
#include "fixedqstringliteral.h"
#include <tuple>
export module URLUtils;

// Leaves a trailing slash
export QUrl resolveUrl(const QUrl &url) {
    // Resolve onedrive:/account/folder1/../folder2 to onedrive:/account/folder2/
    QUrl ret = url;
    QString inputPath = ret.path(QUrl::FullyDecoded);
    if (!inputPath.endsWith(QStringLiteral("/"))) {
        ret.setPath(inputPath + QStringLiteral("/"));
    }
    return ret.resolved(QUrl(QStringLiteral(".")));
}

export QString pathWithoutAccName(const QUrl &url, QUrl::ComponentFormattingOptions options) {
    return resolveUrl(url).path(options).split(QStringLiteral("/")).mid(2).join(QStringLiteral("/"));
}

export int urlSize(const QUrl &url)
{
    return resolveUrl(url).path(QUrl::FullyDecoded).split(QStringLiteral("/")).size();
}

export std::tuple<QUrl, QString> splitLastPart(const QUrl &url)
{
    QUrl newUrl = resolveUrl(url);
    QStringList splitPath = newUrl.path(QUrl::FullyDecoded).split(QStringLiteral("/"));
    // Needs at least 3 components: onedrive:/account/
    if (splitPath.size() < 3) {
        return std::tuple<QUrl, QString>(url, QString());
    }
    splitPath.removeLast();
    QString lastPart = splitPath.takeLast();
    newUrl.setPath(splitPath.join(QStringLiteral("/")), QUrl::DecodedMode);
    return std::tuple<QUrl, QString>(newUrl, lastPart);
}
   
export QString specialUriToPath(const QUrl &specialUri)
{
    QString drivePath = pathWithoutAccName(specialUri, QUrl::FullyDecoded);
    if (drivePath.endsWith(QStringLiteral("/"))) {
        drivePath = drivePath.chopped(1);
    }
    if (drivePath.isEmpty()) {
        return QStringLiteral("/drive/root");
    }
    return QStringLiteral("/drive/root:/") + drivePath + QStringLiteral(":");
}

export bool isAddressable(const QUrl &specialUrl) {
    // At least onedrive:/account/file/
    return urlSize(specialUrl) >= 4;
}

export bool atLeastTopLevel(const QUrl &specialUrl) {
    // At least onedrive:/account/
    return urlSize(specialUrl) >= 3;
}

