// SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri<b.gomes.negri@gmail.com>
// SPDX-FileCopyrightText: 2013-2014 Daniel Vrátil <dvratil@redhat.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later
#include <QCoreApplication>

import OnedriveWorker;

class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.worker.onedrive" FILE "onedrive.json")
};

extern "C" {
    int Q_DECL_EXPORT kdemain(int argc, char **argv)
    {
        QCoreApplication app(argc, argv);
        app.setApplicationName(QStringLiteral("kio_onedrive"));

        if (argc != 4) {
            fprintf(stderr, "Usage: kio_onedrive protocol domain-socket1 domain-socket2\n");
            exit(-1);
        }

        OnedriveWorker worker(argv[1], argv[2], argv[3]);
        worker.dispatchLoop();
        return 0;
    }
}
#include "kio_onedrive.moc"
