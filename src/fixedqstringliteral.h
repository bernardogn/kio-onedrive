// Copyright (C) 2016 The Qt Company Ltd.
// Copyright (C) 2020 Intel Corporation.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

// This is the same as Qt's 'qstringliteral.h' but without the
// 'static' on qMakeStringPrivate. 'static' on functions outside
// classes means the function is only avaliable inside the translation
// unit, however, when using modules, the global module fragment
// is its own separate translation unit (I think, or maybe each header
// is a translation unit?)

#ifndef QSTRINGLITERALFIXED_H
#define QSTRINGLITERALFIXED_H

#include <QtCore/qarraydata.h>
#include <QtCore/qarraydatapointer.h>

// all our supported compilers support Unicode string literals,
// even if their Q_COMPILER_UNICODE_STRING has been revoked due
// to lacking stdlib support. But QStringLiteral only needs the
// core language feature, so just use u"" here unconditionally:

#define QT_UNICODE_LITERAL(str) u"" str

using QStringPrivate = QArrayDataPointer<char16_t>;

namespace QtPrivateFixed {
template <qsizetype N>
Q_ALWAYS_INLINE QStringPrivate qMakeStringPrivate(const char16_t (&literal)[N])
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
    auto str = const_cast<char16_t *>(literal);
    return { nullptr, str, N - 1 };
}
}

#define QStringLiteralF(str) \
    (QString(QtPrivateFixed::qMakeStringPrivate(QT_UNICODE_LITERAL(str)))) \
    /**/

#ifdef QStringLiteral
#undef QStringLiteral
#define QStringLiteral(str) QStringLiteralF(str)
#endif

// So we do not include Qt's qstringliteral.h
#define QSTRINGLITERAL_H

#endif // QSTRINGLITERALFIXED_H
