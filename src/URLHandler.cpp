// SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later
module;
#include "onedrivedebug.h"
#include <KAccounts/AccountsModel>
#include <KAccounts/ServicesModel>
#include <KAccounts/GetCredentialsJob>
#include <Accounts/Account>
#include <QUrl>
#include <QByteArray>
#include <QDateTime>
#include <QHash>
#include <qloggingcategory.h>
#include "fixedqstringliteral.h"
#include <qvariant.h>
#include <QLoggingCategory>
#include <variant>
#include <tuple>
export module URLHandler;
import URLUtils;

export {
    struct Nothing{};
    
    enum URLError {
        NoSuchAccount,
        CredentialsError
    };

    struct RequestInfo {
        QString bearerToken;
        QUrl url;
    };
}

struct ExpiringToken {
    QString token;
    QDateTime expirationDate;
};

export class URLHandler {
public:
    URLHandler() :
    m_accModel()
    {
    }
    
    // Returns the URL to the Drive object ("https://graph.microsoft.com/v1.0/me/drive")
    std::variant<RequestInfo, URLError> driveRequest(const QUrl &url)
    {
        QString accName = get<QString>(specialUriToApi(url));
        auto maybeToken = getTokenForAccName(accName);
        if (std::holds_alternative<URLError>(maybeToken)) {
            return std::get<URLError>(maybeToken);
        } else {
            QString token = std::get<QString>(maybeToken);
            QUrl driveUrl{QStringLiteral("https://graph.microsoft.com/v1.0/me/drive")};
            return RequestInfo{token, driveUrl};
        }
    }
    

    // Converts a onedrive:/ KIO URI to a Onedrive driveItem API address + bearer token
    // See https://learn.microsoft.com/en-us/onedrive/developer/rest-api/concepts/addressing-driveitems
    std::variant<RequestInfo, URLError> specialUriToRequest(const QUrl &specialUri) {
        auto apiInfo = specialUriToApi(specialUri);
        QString accName = get<0>(apiInfo);
        QUrl driveUrl = get<1>(apiInfo);
        auto maybeToken = getTokenForAccName(accName);
        if (std::holds_alternative<URLError>(maybeToken)) {
            std::variant<RequestInfo, URLError> ret(std::get<URLError>(maybeToken));
            return ret;
        } else {
            QString token = std::get<QString>(maybeToken);
            RequestInfo reqInfo;
            reqInfo.bearerToken = token;
            reqInfo.url = driveUrl;
            return std::variant<RequestInfo, URLError>(reqInfo);
        }
    }
    
    std::tuple<QString,QUrl> specialUriToApi(const QUrl &specialUri) {
        QUrl driveUrl;
        driveUrl.setScheme(QStringLiteral("https"));
        driveUrl.setHost(QStringLiteral("graph.microsoft.com"));
        QUrl resolvedUri = resolveUrl(specialUri);
        // Parse the onedrive: uri
        // We expect it to be like onedrive:/kaccount-id/folder/folder
        // We want to get the folder/folder and kaccount-id
        QStringList splitPath = resolvedUri.path(QUrl::FullyDecoded).split(QStringLiteral("/"));
        // We need to remove the trailing slash, if it is there
        QString path = specialUriToPath(specialUri);
        QString accName = splitPath.at(1);
        driveUrl.setPath(QStringLiteral("/v1.0/me") + path, QUrl::DecodedMode);
        return std::make_tuple(accName, driveUrl);
    }
    
    QList<QString> getAccNames() {
        QList<QString> ret;
        for (int i = 0; i < m_accModel.rowCount(); i++) {
            QModelIndex idx = m_accModel.index(i);
            if (!isAccGood(idx, Nothing())) {
                continue;
            }
            ret.append(m_accModel.data(idx, KAccounts::AccountsModel::DisplayNameRole).toString());
        }
        return ret;
    }

private:
    bool isAccGood(QModelIndex idx, std::variant<QString, Nothing> maybeName) {
        QString provider = m_accModel.data(idx, KAccounts::AccountsModel::ProviderNameRole).toString();
        if (provider != QStringLiteral("microsoft")) {
            return false;
        }
        if (!m_accModel.data(idx, KAccounts::AccountsModel::EnabledRole).toBool()) {
            return false;
        }
        if (std::holds_alternative<QString>(maybeName)) {
            QString accName = std::get<QString>(maybeName);
            QString thisAccName = m_accModel.data(idx, KAccounts::AccountsModel::DisplayNameRole).toString();
            if (thisAccName != accName) {
                return false;
            }
        }
        
        KAccounts::ServicesModel *services = m_accModel.data(idx, KAccounts::AccountsModel::ServicesRole).value<KAccounts::ServicesModel*>();
        for (int i = 0; i < services->rowCount(); i++) {
            QModelIndex servicesModelIndex = services->index(i);
            if (!services->data(servicesModelIndex, KAccounts::ServicesModel::Roles::EnabledRole).toBool()) {
                continue;
            }
            QString serviceName = services->data(servicesModelIndex, KAccounts::ServicesModel::NameRole).toString();
            if (serviceName == QStringLiteral("microsoft-onedrive")) {
                return true;
            }
        }
        return false;
    }
    
    std::variant<QString, URLError> getTokenForAccName(QString accName) {
        if (m_accountTokenCache.contains(accName)) {
            ExpiringToken cachedToken = m_accountTokenCache.value(accName);
            QDateTime now = QDateTime::currentDateTimeUtc();
            if (now < cachedToken.expirationDate) {
                return cachedToken.token;
            }
        }
        
        URLError err = URLError::NoSuchAccount;
        bool has_err = true;
        QString token = QString();
        for (int i = 0; i < m_accModel.rowCount(); i++) {
            QModelIndex modelIndex = m_accModel.index(i);
            if (!isAccGood(modelIndex, accName)) {
                continue;
            }
            // We have found our account! Now get that bearer token
            has_err = false;
            auto job = KAccounts::GetCredentialsJob(m_accModel.data(modelIndex, KAccounts::AccountsModel::IdRole).value<Accounts::AccountId>(), nullptr);
            if (!job.exec()) {
                qCWarning(ONEDRIVE) << "Error with GetCredentialsJob: " << job.errorString();
                err = URLError::CredentialsError;
                has_err = true;
            } else {
                QVariantMap data = job.credentialsData();
                token = data.value(QStringLiteral("AccessToken")).toString();
                ExpiringToken expToken;
                expToken.token = token;
                // We give ourselves a few seconds of leeway
                expToken.expirationDate = QDateTime::currentDateTimeUtc().addSecs(data.value(QStringLiteral("ExpiresIn")).toInt() - 20);
                qCDebug(ONEDRIVE) << "Token for account" << accName << "expires at" << expToken.expirationDate;
                m_accountTokenCache[accName] = expToken;
                // The access token is a secret (one that expires in ~15 minutes)
                // Only uncomment this line if you wish to experiment with the Onedrive API.
                // Debug lines go to journalctl!!
                //qCDebug(ONEDRIVE) << "acctoken:" << token;
            }
            break;
        }
        if (has_err) {
            return std::variant<QString, URLError>(err);
        } else {
            return std::variant<QString, URLError>(token);
        }
    }

    KAccounts::AccountsModel m_accModel;
    QHash<QString, ExpiringToken> m_accountTokenCache;
};
