// SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri<b.gomes.negri@gmail.com>
// SPDX-FileCopyrightText: 2013-2014 Daniel Vrátil <dvratil@redhat.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

module;
#include <QDateTime>
#include <QJsonObject>
#include <QString>
#include "fixedqstringliteral.h"
#include <QJsonDocument>
#include <KIO/UDSEntry>
#include <sys/stat.h>
export module DriveItem;
import URLUtils;

export QJsonObject parentRefAndName(const QUrl &url)
{
    auto splitUrl = splitLastPart(url);
    QUrl folder = std::get<QUrl>(splitUrl);
    QString name = std::get<QString>(splitUrl);
    QString parentPath = pathWithoutAccName(folder, QUrl::FullyDecoded);
    return QJsonObject{
        {QStringLiteral("parentReference"),
            QJsonObject{{QStringLiteral("path"), parentPath}}
        },
        {QStringLiteral("name"), name}
    };
}

export bool isFile(const QJsonObject &json) {
    return json.contains(QStringLiteral("file"));
}

export QString mimeForDriveItem(const QJsonObject &obj) {
    return obj.value(QStringLiteral("file")).toObject().value(QStringLiteral("mimeType")).toString();
}


QDateTime createdTimeItem(const QJsonObject &json) {
    QString dateStr = json.value(QStringLiteral("fileSystemInfo")).toObject().value(QStringLiteral("createdDateTime")).toString();
    return QDateTime::fromString(dateStr, Qt::ISODateWithMs);
}

export QDateTime modifiedTimeItem(const QJsonObject &json) {
    QString dateStr = json.value(QStringLiteral("fileSystemInfo")).toObject().value(QStringLiteral("lastModifiedDateTime")).toString();
    return QDateTime::fromString(dateStr, Qt::ISODateWithMs);
}

export KIO::UDSEntry jsonToUdsEntry(const QJsonObject &json) {
    // The format is described at https://learn.microsoft.com/en-us/onedrive/developer/rest-api/resources/driveitem
    KIO::UDSEntry entry;
    entry.fastInsert(KIO::UDSEntry::UDS_NAME, json.value(QStringLiteral("name")).toString());
    // Below line copied from kio-gdrive
    entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
    if (isFile(json)) {
        entry.fastInsert(KIO::UDSEntry::UDS_SIZE, json.value(QStringLiteral("size")).toInteger());
        entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFREG);
        entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, mimeForDriveItem(json));
    } else {
        entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
    }
    entry.fastInsert(KIO::UDSEntry::UDS_MODIFICATION_TIME, modifiedTimeItem(json).toSecsSinceEpoch());
    entry.fastInsert(KIO::UDSEntry::UDS_CREATION_TIME, createdTimeItem(json).toSecsSinceEpoch());
    return entry;
}

export QJsonDocument newDriveFolder(QString name)
{
    QJsonObject obj;
    obj.insert(QStringLiteral("folder"), QJsonValue{QJsonObject{}});
    obj.insert(QStringLiteral("name"), QJsonValue{name});
    obj.insert(QStringLiteral("@microsoft.graph.conflictBehavior"), QJsonValue{QStringLiteral("fail")});
    return QJsonDocument{obj};
}

export KIO::UDSEntry folderEntry(QString name) {
    KIO::UDSEntry entry;
    entry.fastInsert(KIO::UDSEntry::UDS_NAME, name);
    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
    return entry;
}

export KIO::UDSEntry dotEntry() {
    return folderEntry(QStringLiteral("."));
}

export KIO::UDSEntry dotDotEntry() {
    return folderEntry(QStringLiteral(".."));
}
