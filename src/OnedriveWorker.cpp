// SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri<b.gomes.negri@gmail.com>
// SPDX-FileCopyrightText: 2013-2014 Daniel Vrátil <dvratil@redhat.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later
module;
#include "onedriveversion.h"
#include <kio/global.h>
#include <KIO/WorkerBase>
#include <QEventLoop>
#include <QUrl>
#include <QUrlQuery>
#include <QDateTime>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTemporaryFile>
#include <QJsonObject>
#include <QString>
#include "fixedqstringliteral.h"
#include <qvariant.h>
#include <variant>
#include <algorithm>
#include <unistd.h>
#include "onedrivedebug.h"
export module OnedriveWorker;
import URLUtils;
import URLHandler;
import DriveItem;

void deleteReply(QNetworkReply *reply) {
    reply->deleteLater();
}

enum FileFolderEither {
    File, Folder, Either
};

// This is important because Qt APIs expect you to remember to call
// deleteLater on QNetworkReply s, but I'd rather use RAII and have
// the compiler call it for me.
typedef std::unique_ptr<QNetworkReply, decltype(&deleteReply)> ReplyPtr;
ReplyPtr ownReply(QNetworkReply *reply) {
    return ReplyPtr(reply, deleteReply);
}

// Qt doesn't put the HTTP method inside QNetworkRequest
enum HttpMethod
{
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
};

void setJsonContentType(QNetworkRequest &req)
{
    req.setHeader(QNetworkRequest::ContentTypeHeader, QVariant{QStringLiteral("application/json")});
}

int replyStatus(const QNetworkReply &reply)
{
    return reply.attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
}

bool hasError(const QNetworkReply &reply)
{
    if (replyStatus(reply) == 0) {
        return true;
    }
    if (!reply.isFinished()) {
        return true;
    }
    //reply->error() sometimes returns stuff such as a 404 as an error
    QList<QNetworkReply::NetworkError> acceptableErrors{
        QNetworkReply::NoError,
        QNetworkReply::ContentNotFoundError,
        QNetworkReply::ContentConflictError,
        QNetworkReply::ContentGoneError
    };
    return !acceptableErrors.contains(reply.error());
}

QString overrideSetting(KIO::JobFlags flags)
{
    return flags.testFlag(KIO::JobFlag::Overwrite) ? QStringLiteral("replace") : QStringLiteral("fail");
}

// These are the only attributes kio-onedrive uses.
const QUrlQuery selectedInfo{{QStringLiteral("select"), QStringLiteral("name,size,file,fileSystemInfo,@microsoft.graph.downloadUrl")}};

// A quarter of a second
constexpr useconds_t progressWaitAmount = 250 * 1000;

// 20 seconds
constexpr std::chrono::milliseconds defaultTimeout{20l * 1000l};

// This follows the recommendation at
// https://learn.microsoft.com/en-us/sharepoint/dev/general-development/how-to-avoid-getting-throttled-or-blocked-in-sharepoint-online
// ISV means "Independent Software Vendor"
const QString userAgent = QStringLiteral("ISV|kio-onedrive|kio-onedrive/" ONEDRIVE_VERSION_STRING);

const QString overridePolicy = QStringLiteral("@microsoft.graph.conflictBehavior");

export class OnedriveWorker : public KIO::WorkerBase {
public:
    OnedriveWorker(const QByteArray &protocol, const QByteArray &pool_socket, const QByteArray &app_socket)
    : WorkerBase("onedrive", pool_socket, app_socket)
    , m_accessManager()
    , m_urlHandler()
    {
        Q_UNUSED(protocol);
        m_accessManager.connectToHostEncrypted(QStringLiteral("graph.microsoft.com"));
        m_accessManager.setTransferTimeout(defaultTimeout);
        qCDebug(ONEDRIVE) << "KIO Onedrive ready: version" << ONEDRIVE_VERSION_STRING;
    }
    
    KIO::WorkerResult setModificationTime(const QUrl &url, const QDateTime &mtime)
    {
        qCDebug(ONEDRIVE) << "Operation: setting mtime of" << url;
        // Needs to be at least onedrive:/account/folder/
        if (!isAddressable(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_SETTIME);
        }
        
        auto maybeReqInfo = m_urlHandler.specialUriToRequest(url);
        if (std::holds_alternative<URLError>(maybeReqInfo)) {
            return resFromURLError(std::get<URLError>(maybeReqInfo));
        }
        QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo));
        setJsonContentType(req);
        
        auto reqObj = QJsonObject{
            {QStringLiteral("fileSystemInfo"), QJsonObject{{QStringLiteral("lastModifiedDateTime"), mtime.toString(Qt::ISODateWithMs)}}}
        };
        QByteArray reqBody = QJsonDocument{reqObj}.toJson(QJsonDocument::Compact);
        ReplyPtr reply = doNetworkWithMethod(req, HttpMethod::PATCH, reqBody);
        int status = replyStatus(*reply);
        if (status == 200) {
            return KIO::WorkerResult::pass();
        } else if (status == 440) {
            return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST);
        } else {
            logNetError(*reply);
            return netError();
        }
    }
    
    KIO::WorkerResult put(const QUrl &url, int permissions, KIO::JobFlags flags)
    {
        Q_UNUSED(permissions);
        const int minForSimpleXfer = 4 * 1024 * 1024;
        qCDebug(ONEDRIVE) << "Operation: writing to" << url;
        // Needs to be at least onedrive:/account/folder/
        if (!isAddressable(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_WRITE);
        }
        // The onedrive API requires us to know how much data we will upload, which means
        // we need to hold all the data either in memory or in disk
        QTemporaryFile totalData;
        totalData.open();
        qint64 totalBytes = 0;
        QByteArray thisData;
        do {
            dataReq();
            // KIO overwrites whatever was in the buffer with the copy constructor
            // https://invent.kde.org/frameworks/kio/-/blob/ee05bf303d334465f145fc8c8aeb5ad5255b1b75/src/core/connection.cpp#L225
            int bytesRead = readData(thisData);
            if (bytesRead == 0) {
                break;
            } else if (bytesRead < 0) {
                return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
            }
            if (wasKilled()) {
                return KIO::WorkerResult::fail(KIO::ERR_ABORTED);
            }
            totalData.write(thisData);
            totalBytes += thisData.size();
        } while (thisData.size() != 0);
        totalData.seek(0);
        
        auto maybeReqInfo = m_urlHandler.specialUriToRequest(url);
        if (std::holds_alternative<URLError>(maybeReqInfo)) {
            return resFromURLError(std::get<URLError>(maybeReqInfo));
        }
        
        if (totalBytes < minForSimpleXfer) {
            QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo), QStringLiteral(u"/content"));
            QUrl reqUrl = req.url();
            reqUrl.setQuery(QUrlQuery{{overridePolicy, overrideSetting(flags)}});
            req.setUrl(reqUrl);
            ReplyPtr reply = doNetworkWithMethod(req, HttpMethod::PUT, &totalData);
            int status = replyStatus(*reply);
            if (status == 201 || status == 200) {
                
            } else if (status == 409) {
                return KIO::WorkerResult::fail(KIO::ERR_FILE_ALREADY_EXIST);
            } else {
                logNetError(*reply);
                return netError();
            }
        } else {
            // Microsoft recommends a 10MiB size for faster connections, and
            // a 5-10 for other
            // We may tune this as needed, but it needs to be a multiple of 320 KiB
            // 7.5 MiB
            constexpr qint64 recommendedFragSize = (7 * 1024 * 1024) + (512 * 1024);
            static_assert(recommendedFragSize % (320 * 1024) == 0, "recommendedFragSize must be a multiple of 320 KiB");
            // We do exponential back off in case of error
            int waitAmount = 1;
            QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo), QStringLiteral("/createUploadSession"));
            setJsonContentType(req);
            QJsonObject jsonReqBody{{QStringLiteral("item"), QJsonObject{{overridePolicy, overrideSetting(flags)}}}};
            ReplyPtr reply = doNetworkWithMethod(req, HttpMethod::POST, QJsonDocument{jsonReqBody}.toJson(QJsonDocument::Compact));
            int status = replyStatus(*reply);
            if (status != 200) {
                logNetError(*reply);
                return netError();
            }
            auto maybeUploadSession = parseJson(*reply);
            if (!std::holds_alternative<QJsonDocument>(maybeUploadSession)) {
                logNetError(*reply);
                return netError();
            }
            const QUrl uploadSessionUrl{std::get<QJsonDocument>(maybeUploadSession).object().value(QStringLiteral("uploadUrl")).toString()};
            // This URL is pre-authenticated
            qint64 thisByte = 0;
            while (true) {
                totalData.seek(thisByte);
                QNetworkRequest req{uploadSessionUrl};
                req.setHeader(QNetworkRequest::UserAgentHeader, userAgent);
                if (wasKilled()) {
                    // Normally, SlaveBase already sets an alarm for us at the
                    // same time wasKilled is set to true.
                    // However, since we use sleep(), which may use alarm() in
                    // its implementation, the alarm set by SlaveBase may have been
                    // canceled (because alarm() cancels all other alarms)
                    alarm(5);
                    doNetworkWithMethod(req, HttpMethod::DELETE);
                    // We don't really care if the deletion failed or not
                    return KIO::WorkerResult::fail(KIO::ERR_ABORTED);
                }
                const qint64 bytesRemaining = totalBytes - thisByte;
                const qint64 bytesToUpload = std::min(bytesRemaining, recommendedFragSize);
                req.setHeader(QNetworkRequest::ContentLengthHeader, bytesToUpload);
                const QString rangeString = QStringLiteral("bytes ") + QString::number(thisByte) +
                    QStringLiteral("-") + QString::number(thisByte + bytesToUpload - 1) +
                    QStringLiteral("/") + QString::number(totalBytes);
                req.setRawHeader("Content-Range", rangeString.toUtf8());
                // TODO: write a QIODevice implementation that returns the bytes of another
                // QIODevice but starting at a certain byte and returning EOF at a certain point
                // and another that uses a list of arrays as backing storage
                // This would save memory copies to upload something, because right now it uses 3
                // on a system with a tmpfs tmpdir
                qCDebug(ONEDRIVE) << "Uploading chunk of size" << bytesToUpload;
                ReplyPtr reply = doNetworkWithMethod(req, HttpMethod::PUT, totalData.read(bytesToUpload));
                int status = replyStatus(*reply);
                if (status >= 500 && status < 600) {
                    // Exponential back-off
                    // We check if we were killed before sleeping because another signal will not interrupt
                    // sleep if we were already killed
                    if (wasKilled()) {
                        alarm(5);
                        doNetworkWithMethod(req, HttpMethod::DELETE);
                        return KIO::WorkerResult::fail(KIO::ERR_ABORTED);
                    }
                    qCDebug(ONEDRIVE) << "Server returned 5xx, sleeping for" << waitAmount << "seconds";
                    if (waitAmount >= 32) {
                        logNetError(*reply);
                        return netError();
                    }
                    sleep(waitAmount);
                    waitAmount *= 2;
                } else if (status == 202) {
                    waitAmount = 1;
                    thisByte += bytesToUpload;
                } else if (status == 201 || status == 200) {
                    break;
                } else if (status == 409) {
                    return KIO::WorkerResult::fail(KIO::ERR_FILE_ALREADY_EXIST);
                    // TODO: handle 404 errors (means the upload session expired)
                } else {
                    logNetError(*reply);
                    return netError();
                }
            }
        }
        qCDebug(ONEDRIVE) << "Upload finished";
        if (hasMetaData(QStringLiteral("modified"))) {
            setModificationTime(url, QDateTime::fromString(metaData(QStringLiteral("modified")), Qt::ISODate));
        }
        return KIO::WorkerResult::pass();
    }

    KIO::WorkerResult copy(const QUrl &src, const QUrl &dest, int permissions, KIO::JobFlags flags)
    {
        Q_UNUSED(permissions)
        qCDebug(ONEDRIVE) << "Operation: copying" << src << "to" << dest;
        // Needs to be at least onedrive:/account/folder/
        if (!isAddressable(src) || !isAddressable(dest)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_RENAME);
        }
        if (!sameAccount(src, dest)) {
            return KIO::WorkerResult::fail(KIO::ERR_UNSUPPORTED_ACTION);
        }
        
        if (flags.testFlag(KIO::JobFlag::Overwrite)) {
            qCDebug(ONEDRIVE) << "Tried to overwrite on copy";
            return KIO::WorkerResult::fail(KIO::ERR_UNSUPPORTED_ACTION);
        }
        
        // We need to obtain the size here for proper progress reporting.
        auto maybeFileInfo = fileData(src, Either);
        if (!std::holds_alternative<QJsonObject>(maybeFileInfo)) {
            return std::get<KIO::WorkerResult>(maybeFileInfo);
        }
        QJsonObject fileInfo = std::get<QJsonObject>(maybeFileInfo);
        qint64 size = fileInfo.value(QStringLiteral("size")).toInteger();
        
        QJsonObject objBody = parentRefAndName(dest);
        QByteArray reqBody = QJsonDocument{objBody}.toJson(QJsonDocument::Compact);
        auto maybeReqInfo = m_urlHandler.specialUriToRequest(src);
        if (std::holds_alternative<URLError>(maybeReqInfo)) {
            return resFromURLError(std::get<URLError>(maybeReqInfo));
        }
        QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo), QStringLiteral("/copy"));
        setJsonContentType(req);
        ReplyPtr reply = postNetwork(req, reqBody);
        int status = replyStatus(*reply);
        if (status == 400) {
            return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST);
        } else if (status == 409) {
            return KIO::WorkerResult::fail(KIO::ERR_FILE_ALREADY_EXIST);
        } else if (status == 200) {
            return KIO::WorkerResult::pass();
        } else if (status != 202) {
            logNetError(*reply);
            return netError();
        }
        
        // Now here is the fun part
        // OneDrive API has a mechanism where the HTTP call returns immediately with 202
        // but the result is asynchronous. For this worker, only copy has this
        // behaviour
        QUrl statusUrl = reply->header(QNetworkRequest::LocationHeader).toUrl();
        bool finished = false;
        totalSize(size);
        while (!finished) {
            if (wasKilled()) {
                // Can't return aborted because we don't actually abort the copy
                return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
            }
            QNetworkRequest statusQuery{statusUrl};
            ReplyPtr statusReply = getNetwork(statusQuery);
            auto maybeJson = parseJson(*statusReply);
            if (!std::holds_alternative<QJsonDocument>(maybeJson)) {
                return netError();
            }
            QJsonObject statusObj = std::get<QJsonDocument>(maybeJson).object();
            QString statusStr = statusObj.value(QStringLiteral("status")).toString();
            if (statusStr == QStringLiteral("completed")) {
                finished = true;
                break;
            } else if (statusStr == QStringLiteral("failed")) {
                QString errorCode = statusObj.value(QStringLiteral("errorCode")).toString();
                if (errorCode.startsWith(QStringLiteral("RelationshipNameAlreadyExists"))) {
                    qCDebug(ONEDRIVE) << "Failing the copy because destination already exists";
                    return KIO::WorkerResult::fail(KIO::ERR_FILE_ALREADY_EXIST);
                } else {
                    qCWarning(ONEDRIVE) << "Error code from Microsoft when copying:" << errorCode;
                    return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
                }
            } else if (statusStr == QStringLiteral("inProgress")) {
                double percent = statusObj.value(QStringLiteral("percentageComplete")).toDouble();
                processedSize((qint64)((double)size * (percent / 100.0)));
            } else if (statusStr == QStringLiteral("notStarted")) {
            } else {
                qCWarning(ONEDRIVE) << "Unknown status:" << statusStr;
                return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
            }
            usleep(progressWaitAmount);
        }
        // We need to set the mtime on the copy
        // This does mean copy takes a minimum of 4 requests
        // Don't bother with the return because we shouldn't fail the whole
        // copy because we couldn't set the mtime
        setModificationTime(resolveUrl(dest), modifiedTimeItem(fileInfo));
        return KIO::WorkerResult::pass();
    }
    
    KIO::WorkerResult rename(const QUrl &src, const QUrl &dest, KIO::JobFlags flags)
    {
        qCDebug(ONEDRIVE) << "Operation: moving" << src << "to" << dest;
        // Needs to be at least onedrive:/account/folder/
        if (!isAddressable(src) || !isAddressable(dest)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_RENAME);
        }
        if (!sameAccount(src, dest)) {
            return KIO::WorkerResult::fail(KIO::ERR_UNSUPPORTED_ACTION);
        }
        
        auto maybeFileInfo = fileData(src, Either);
        if (!std::holds_alternative<QJsonObject>(maybeFileInfo)) {
            return std::get<KIO::WorkerResult>(maybeFileInfo);
        }
        QDateTime mTime = modifiedTimeItem(std::get<QJsonObject>(maybeFileInfo));
        
        QJsonObject objBody = parentRefAndName(dest);
        objBody.insert(overridePolicy, overrideSetting(flags));
        auto maybeReqInfo = m_urlHandler.specialUriToRequest(src);
        if (std::holds_alternative<URLError>(maybeReqInfo)) {
            return resFromURLError(std::get<URLError>(maybeReqInfo));
        }
        QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo));
        setJsonContentType(req);
        ReplyPtr reply = doNetworkWithMethod(req, HttpMethod::PATCH, QJsonDocument{objBody}.toJson(QJsonDocument::Compact));
        int status = replyStatus(*reply);
        // Fails with 400 if src does not exist
        // Fails with 409 (Conflict) if dest exists
        // Succeeds with 200
        if (status == 400) {
            return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST);
        } else if (status == 409) {
            // If trying to rename a folder, will return wrong error code
            return KIO::WorkerResult::fail(KIO::ERR_FILE_ALREADY_EXIST);
        } else if (status == 200) {
            setModificationTime(resolveUrl(dest), mTime);
            return KIO::WorkerResult::pass();
        } else {
            logNetError(*reply);
            return netError();
        }
    }
    
    KIO::WorkerResult del(const QUrl &url, bool isFile)
    {
        qCDebug(ONEDRIVE) << "Operation: deleting" << url;
        // Needs to be at least onedrive:/account/folder/
        if (!isAddressable(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_RMDIR);
        }
        auto maybeReqInfo = m_urlHandler.specialUriToRequest(url);
        if (std::holds_alternative<URLError>(maybeReqInfo)) {
            return resFromURLError(std::get<URLError>(maybeReqInfo));
        }
        QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReqInfo));
        // Make sure it is what we expect
        auto maybeFileData = fileData(url, isFile ? File : Folder);
        if (std::holds_alternative<KIO::WorkerResult>(maybeFileData)) {
            return std::get<KIO::WorkerResult>(maybeFileData);
        }
        QJsonObject file = std::get<QJsonObject>(maybeFileData);
        if (!isFile && metaData(QStringLiteral("recurse")) != QStringLiteral("true")) {
            // Check if it is empty
            qint64 childCount = file.value(QStringLiteral("folder")).toObject().value(QStringLiteral("childCount")).toInteger();
            if (childCount != 0) {
                // Folder is not empty
                // Is there a better error code?
                return KIO::WorkerResult::fail(KIO::ERR_CANNOT_RMDIR);
            }
        }
        QString eTag = file.value(QStringLiteral("eTag")).toString();
        req.setHeader(QNetworkRequest::IfMatchHeader, QStringList{eTag});
        ReplyPtr reply = delNetwork(req);
        int status = replyStatus(*reply);
        // "No content"
        if (status == 204) {
            return KIO::WorkerResult::pass();
        // "Precondition failed" (our eTag)
        } else if (status == 412) {
            qCWarning(ONEDRIVE) << "The status of the item changed between checking and deleting. Unlucky";
            return KIO::WorkerResult::fail(isFile ? KIO::ERR_CANNOT_DELETE : KIO::ERR_CANNOT_RMDIR);
        } else {
            logNetError(*reply);
            return netError();
        }
    }
    
    KIO::WorkerResult mkdir(const QUrl &url, int permissions)
    {
        Q_UNUSED(permissions)
        qCDebug(ONEDRIVE) << "Operation: creating folder" << url;
        // Needs at least 4 components: onedrive:/account/folder/
        if (!isAddressable(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_MKDIR);
        }
        auto splitUrl = splitLastPart(url);
        QUrl newUrl = std::get<QUrl>(splitUrl);
        QString newFolderName = std::get<QString>(splitUrl);
        auto maybeReq = m_urlHandler.specialUriToRequest(newUrl);
        if (std::holds_alternative<URLError>(maybeReq)) {
            return resFromURLError(std::get<URLError>(maybeReq));
        }
        QByteArray body = newDriveFolder(newFolderName).toJson(QJsonDocument::Compact);
        QNetworkRequest req = createReq(std::get<RequestInfo>(maybeReq), QStringLiteral("/children"));
        setJsonContentType(req);
        ReplyPtr reply = postNetwork(req, body);
        int status = replyStatus(*reply);
        // Created and conflict, respectively
        if (status == 201) {
            return KIO::WorkerResult::pass();
        } else if (status == 409) {
            return KIO::WorkerResult::fail(KIO::ERR_DIR_ALREADY_EXIST);
        } else {
            logNetError(*reply);
            return netError();
        }
    }
    
    KIO::WorkerResult fileSystemFreeSpace(const QUrl &url)
    {
        qCDebug(ONEDRIVE) << "Operation: checking free space of" << url;
        // Needs to be at least onedrive:/account/
        if (!atLeastTopLevel(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_STAT);
        }
        auto maybeReq = m_urlHandler.driveRequest(url);
        if (std::holds_alternative<URLError>(maybeReq)) {
            return resFromURLError(std::get<URLError>(maybeReq));
        }
        auto networkReply = getJson(createReq(std::get<RequestInfo>(maybeReq)));
        if (!std::holds_alternative<QJsonDocument>(networkReply)) {
            if (std::holds_alternative<ReplyPtr>(networkReply)) {
                ReplyPtr ownedReply = std::move(std::get<ReplyPtr>(networkReply));
                logNetError(*ownedReply);
            }
            return netError();
        }
        QJsonObject drive = std::get<QJsonDocument>(networkReply).object();
        if (!drive.contains(QStringLiteral("quota"))) {
            // Couldn't find any more suitable error code
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_STAT);
        }
        QJsonObject quota = drive.value(QStringLiteral("quota")).toObject();
        QString availableSize = QString::number(quota.value(QStringLiteral("remaining")).toInteger());
        QString totalSize = QString::number(quota.value(QStringLiteral("total")).toInteger());
        // TODO: send patch to document this
        setMetaData(QStringLiteral("available"), availableSize);
        setMetaData(QStringLiteral("total"), totalSize);
        return KIO::WorkerResult::pass();
    }
    
    KIO::WorkerResult stat(const QUrl &url) {
        qCDebug(ONEDRIVE) << "Operation: reading metadata of" << url;
        auto maybeFile = fileData(url, Either);
        if (std::holds_alternative<KIO::WorkerResult>(maybeFile)) {
            return std::get<KIO::WorkerResult>(maybeFile);
        }
        QJsonObject driveItem = std::get<QJsonObject>(maybeFile);
        statEntry(jsonToUdsEntry(driveItem));
        return KIO::WorkerResult::pass();
    }
    
    KIO::WorkerResult mimetype(const QUrl &url) {
        qCDebug(ONEDRIVE) << "Operation: getting mime of" << url;
        auto file = fileData(url, File);
        if (std::holds_alternative<KIO::WorkerResult>(file)) {
            return std::get<KIO::WorkerResult>(file);
        }
        mimeType(mimeForDriveItem(std::get<QJsonObject>(file)));
        return KIO::WorkerResult::pass();
    }
    
    KIO::WorkerResult get(const QUrl &url) {
        qCDebug(ONEDRIVE) << "Operation: reading" << url;
        auto file = fileData(url, File);
        if (std::holds_alternative<KIO::WorkerResult>(file)) {
            return std::get<KIO::WorkerResult>(file);
        }
        QJsonObject fileObj = std::get<QJsonObject>(file);
        mimeType(mimeForDriveItem(fileObj));
        // Now let's get data
        QUrl downloadUrl = QUrl(fileObj.value(QStringLiteral("@microsoft.graph.downloadUrl")).toString(), QUrl::StrictMode);
        if (!downloadUrl.isValid()) {
            qCWarning(ONEDRIVE) << "Error when parsing Microsoft's URL:" << downloadUrl.errorString();
            return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
        }
        // this URL does not need to be authenticated
        QNetworkRequest req{downloadUrl};
        QEventLoop loop;
        ReplyPtr reply = ownReply(m_accessManager.get(req));
        while (!reply->isFinished()) {
            if (wasKilled()) {
                return KIO::WorkerResult::fail(KIO::ERR_ABORTED);
            }
            loop.processEvents({QEventLoop::ExcludeUserInputEvents, QEventLoop::WaitForMoreEvents});
            if (reply->bytesAvailable() > 0) {
                data(reply->read(reply->bytesAvailable()));
            }
        }
        if (reply->error() != QNetworkReply::NoError) {
            logNetError(*reply);
            return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
        }
        data(QByteArray());
        return KIO::WorkerResult::pass();
    }

    KIO::WorkerResult listDir(const QUrl &protocolUrl) {
        qCDebug(ONEDRIVE) << "Operation: listing" << protocolUrl;
        if (resolveUrl(protocolUrl).path(QUrl::FullyDecoded) == QStringLiteral("/")) {
            for (QString accName : m_urlHandler.getAccNames()) {
                listEntry(folderEntry(accName));
            }
            listEntry(dotEntry());
            return KIO::WorkerResult::pass();
        }
        auto maybeOnedriveHttpInfo = getReqInfoAndError(protocolUrl);
        if (std::holds_alternative<KIO::WorkerResult>(maybeOnedriveHttpInfo)) {
            return std::get<KIO::WorkerResult>(maybeOnedriveHttpInfo);
        }
        auto onedriveHttpInfo = std::get<RequestInfo>(maybeOnedriveHttpInfo);
        QNetworkRequest request = createReq(onedriveHttpInfo, QStringLiteral("/children"), selectedInfo);
        bool moreItems = false;
        do {
            auto networkResult = getJson(request);
            if (!std::holds_alternative<QJsonDocument>(networkResult)) {
                return notFoundOrAnother(networkResult);
            }
            QJsonObject jsonObj = std::get<QJsonDocument>(networkResult).object();
            
            QString maybeNextItems = jsonObj.value(QStringLiteral("@odata.nextLink")).toString();
            moreItems = !maybeNextItems.isNull();
            if (moreItems) {
                QUrl nextItems{maybeNextItems, QUrl::StrictMode};
                onedriveHttpInfo.url = nextItems;
                request = createReq(onedriveHttpInfo, selectedInfo);
            }
            
            QJsonArray items = jsonObj.value(QStringLiteral("value")).toArray();
            if (items.size() == 0) {
                // When trying to list a file, the API returns a perfectly valid, empty list
                // We need to check if we are trying to list a file or an empty folder
                auto maybeDir = fileData(protocolUrl, Folder);
                if (std::holds_alternative<KIO::WorkerResult>(maybeDir)) {
                    return std::get<KIO::WorkerResult>(maybeDir);
                }
            }
            for (auto iter = items.constBegin(); iter != items.constEnd(); ++iter) {
                listEntry(jsonToUdsEntry(iter->toObject()));
            }
        } while(moreItems);
        listEntry(dotEntry());
        listEntry(dotDotEntry());
        return KIO::WorkerResult::pass();
    }

    ~OnedriveWorker() {
        closeConnection();
    }

private:
    KIO::WorkerResult netError() {
        return KIO::WorkerResult::fail(KIO::ERR_SERVICE_NOT_AVAILABLE, QStringLiteral("Could not fetch data from server"));
    }
    
    std::variant<QJsonObject,KIO::WorkerResult> fileData(const QUrl &url, FileFolderEither objType) {
        // "onedrive:/" is a dir as well as "onedrive:/account/"
        // 4 minimum parts for files because /account/file/ is the minimum
        // 3 minimum parts for dirs because /account/ is the minimum (root folder counts)
        if (objType == File ? !isAddressable(url) : !atLeastTopLevel(url)) {
            return KIO::WorkerResult::fail(KIO::ERR_IS_DIRECTORY);
        }
        auto maybeOnedriveHttpInfo = getReqInfoAndError(url);
        if (std::holds_alternative<KIO::WorkerResult>(maybeOnedriveHttpInfo)) {
            return std::get<KIO::WorkerResult>(maybeOnedriveHttpInfo);
        }
        auto onedriveHttpInfo = std::get<RequestInfo>(maybeOnedriveHttpInfo);
        auto networkResult = getJson(createReq(onedriveHttpInfo, selectedInfo));
        if (!std::holds_alternative<QJsonDocument>(networkResult)) {
            return notFoundOrAnother(networkResult);
        }
        QJsonObject fileObj = std::get<QJsonDocument>(networkResult).object();
        switch(objType) {
            case File:
                if (!isFile(fileObj)) {
                    return KIO::WorkerResult::fail(KIO::ERR_IS_DIRECTORY);
                }
                break;
            case Folder:
                if (isFile(fileObj)) {
                    return KIO::WorkerResult::fail(KIO::ERR_IS_FILE);
                }
                break;
            default:
                break;
        }
        return fileObj;
    }
    
    KIO::WorkerResult resFromURLError(const URLError &err)
    {
        switch (err) {
            case URLError::NoSuchAccount:
                return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST);
                break;
            case URLError::CredentialsError:
                return KIO::WorkerResult::fail(KIO::ERR_CANNOT_LOGIN, QStringLiteral("Failed to get credentials"));
                break;
            default:
                return KIO::WorkerResult::fail(KIO::ERR_UNKNOWN);
                break;
        }
    }
    
    std::variant<KIO::WorkerResult,RequestInfo> getReqInfoAndError(const QUrl &url) {
        auto maybeOnedriveHttpInfo = m_urlHandler.specialUriToRequest(url);
        if (std::holds_alternative<URLError>(maybeOnedriveHttpInfo)) {
            return resFromURLError(std::get<URLError>(maybeOnedriveHttpInfo));
        }
        return std::get<RequestInfo>(maybeOnedriveHttpInfo);
    }
    
    void doNetwork(const QNetworkReply &reply)
    {
        QEventLoop loop;
        while (!reply.isFinished()) {
            // We are not supposed to have user input events, but just in case
            loop.processEvents({QEventLoop::ExcludeUserInputEvents, QEventLoop::WaitForMoreEvents});
        }
    }
    
    ReplyPtr doNetworkWithMethod(const QNetworkRequest &req, HttpMethod method)
    {
        // This starts unitialized
        QNetworkReply *reply;
        switch (method) {
            case HttpMethod::GET:
                reply = m_accessManager.get(req);
                break;
            case HttpMethod::DELETE:
                reply = m_accessManager.deleteResource(req);
                break;
            default:
                qCFatal(ONEDRIVE) << "Programmer error! Tried to call" << __func__ << "with method" << method << ". Please contact the developers.";
                // The application is already dead
                return ReplyPtr(nullptr, deleteReply);
        }
        ReplyPtr ownedReply = ownReply(reply);
        doNetwork(*ownedReply);
        return ownedReply;
    }
    
    template<typename ReqBodyType>
    ReplyPtr doNetworkWithMethod(const QNetworkRequest &req, HttpMethod method, ReqBodyType body)
    {
        QNetworkReply *reply;
        switch(method) {
            case HttpMethod::POST:
                reply = m_accessManager.post(req, body);
                break;
            case HttpMethod::PUT:
                reply = m_accessManager.put(req, body);
                break;
            case HttpMethod::PATCH:
                // Couldn't find any function that supported PATCH
                reply = m_accessManager.sendCustomRequest(req, "PATCH", body);
                break;
            default:
                qCFatal(ONEDRIVE) << "Programmer error! Tried to call" << __func__ << "with method" << method << ". Please contact the developers.";
                return ReplyPtr(nullptr, deleteReply);
        }
        ReplyPtr ownedReply = ownReply(reply);
        doNetwork(*ownedReply);
        return ownedReply;
    }
    
    // The HTTP method really should be specified inside QNetworkRequest
    // instead of depending on what method of QNetworkAccessManager
    // is called
    ReplyPtr getNetwork(const QNetworkRequest &req) {
        return doNetworkWithMethod(req, HttpMethod::GET);
    }
    
    ReplyPtr postNetwork(const QNetworkRequest &req, const QByteArray &body) {
        return doNetworkWithMethod(req, HttpMethod::POST, body);
    }
    
    ReplyPtr delNetwork(const QNetworkRequest &req) {
        return doNetworkWithMethod(req, HttpMethod::DELETE);
    }
    
    std::variant<QJsonDocument, QJsonParseError> parseJson(QNetworkReply &reply) {
        QJsonParseError parseError;
        QByteArray responseBytes = reply.readAll();
        QJsonDocument jsonReply = QJsonDocument::fromJson(responseBytes, &parseError);
        if (parseError.error != QJsonParseError::NoError) {
            qCWarning(ONEDRIVE) << "Error when parsing JSON: " << parseError.errorString();
            return parseError;
        }
        return jsonReply;
    }
    
    void logNetError(QNetworkReply &reply)
    {
        qCWarning(ONEDRIVE) << "Error when making the request:";
        qCWarning(ONEDRIVE) << "Qt Error code" << reply.error();
        qCWarning(ONEDRIVE) << "Http status code" << replyStatus(reply);
        // Try and get Microsoft's error name
        auto maybeJson = parseJson(reply);
        if (std::holds_alternative<QJsonDocument>(maybeJson)) {
            qCWarning(ONEDRIVE) << "Microsoft error code" <<
                std::get<QJsonDocument>(maybeJson).object().value(QStringLiteral("error")).toObject().value(QStringLiteral("code")).toString();
        }
        qCWarning(ONEDRIVE) << reply.errorString();
    }
    
    KIO::WorkerResult notFoundOrAnother(std::variant<QJsonParseError, ReplyPtr, QJsonDocument> &err)
    {
        if (std::holds_alternative<ReplyPtr>(err)) {
            ReplyPtr reply = std::move(std::get<ReplyPtr>(err));
            if (reply->error() == QNetworkReply::ContentNotFoundError) {
                return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST);
            }
            logNetError(*reply);
        }
        if (std::holds_alternative<QJsonDocument>(err)) {
            return KIO::WorkerResult::pass();
        }
        return netError();
    }
    
    std::variant<QJsonParseError, ReplyPtr, QJsonDocument> getJson(const QNetworkRequest &req) {
        ReplyPtr networkReply = getNetwork(req);
        if(networkReply->error() != QNetworkReply::NoError) {
            if (hasError(*networkReply)) {
                logNetError(*networkReply);
            }
            return networkReply;
        }
        auto maybeJsonReply = parseJson(*networkReply);
        // Would be fun if C++ had de-structuring pattern matching (like Python or Haskell)
        if (std::holds_alternative<QJsonDocument>(maybeJsonReply)) {
            return std::get<QJsonDocument>(maybeJsonReply);
        } else {
            return std::get<QJsonParseError>(maybeJsonReply);
        }
    }
    
    QNetworkRequest createReq(QString accessToken, QUrl url) {
        QNetworkRequest req{url};
        req.setRawHeader(QByteArray("Authorization"), (QStringLiteral("bearer ") + accessToken).toUtf8());
        req.setHeader(QNetworkRequest::UserAgentHeader, userAgent);
        qCDebug(ONEDRIVE) << "Making request to " << url.toString(QUrl::FullyEncoded);
        return req;
    }
    
    QNetworkRequest createReq(const RequestInfo &info) {
        return createReq(info.bearerToken, info.url);
    }
    
    QNetworkRequest createReq(const RequestInfo &info, QString extraPath, QUrlQuery extraQuery) {
        QUrl url = info.url;
        url.setPath(url.path(QUrl::FullyEncoded) + extraPath, QUrl::StrictMode);
        QUrlQuery newQuery{url};
        // This causes a compiler error
        // QList<std::pair<QString,QString>> extraQueryParams = extraQuery.queryItems();
        QStringList extraParams = extraQuery.query(QUrl::FullyEncoded).split(newQuery.queryPairDelimiter());
        for (QString param : extraParams) {
            if (param.isEmpty()) {
                continue;
            }
            QStringList splitKeyValue = param.split(newQuery.queryValueDelimiter());
            newQuery.addQueryItem(splitKeyValue.at(0), splitKeyValue.at(1));
        }
        url.setQuery(newQuery.toString(QUrl::FullyEncoded), QUrl::StrictMode);
        return createReq(info.bearerToken, url);
    }
    
    QNetworkRequest createReq(const RequestInfo &info, QString extraPath) {
        return createReq(info, extraPath, QUrlQuery{});
    }
    
    QNetworkRequest createReq(const RequestInfo &info, QUrlQuery queryParams) {
        return createReq(info, QString{}, queryParams);
    }
    
    bool sameAccount(const QUrl &url1, const QUrl &url2)
    {
        QString acc1 = std::get<QString>(m_urlHandler.specialUriToApi(url1));
        QString acc2 = std::get<QString>(m_urlHandler.specialUriToApi(url2));
        return acc1 == acc2;
    }
    
    QNetworkAccessManager m_accessManager;
    URLHandler m_urlHandler;
};

