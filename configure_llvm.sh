# SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
exec bash $SCRIPT_DIR/configure.sh "$@"

