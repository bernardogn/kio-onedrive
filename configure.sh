# SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri<b.gomes.negri@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

export PKG_CONFIG_PATH=$HOME/kde/usr/lib64/pkgconfig
export LD_LIBRARY_PATH=$HOME/kde/usr/lib64
export PATH=$HOME/kde/usr/bin:$PATH
export CMAKE_PREFIX_PATH=$HOME/kde/usr
export CMAKE_MODULE_PATH=$HOME/kde/usr/lib64/cmake:$HOME/kde/usr/lib/cmake
export QT_PLUGIN_PATH=$HOME/kde/usr/lib64/plugins:$HOME/kde/usr/lib/plugins
export XDG_DATA_DIRS=$HOME/kde/usr/share:$HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share
cmake -B . -S $HOME/kde/src/kio-onedrive -G 'Kate - Ninja' '-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON' '-DCMAKE_BUILD_TYPE=RelWithDebInfo' '-DBUILD_WITH_QT6=ON' 'DCMAKE_CXX_FLAGS:STRING=-pipe' '-DCMAKE_INSTALL_PREFIX=$HOME/kde/usr' "$@"
