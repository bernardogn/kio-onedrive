/*
 * SPDX-FileCopyrightText: 2016 Elvis Angelaccio <elvis.angelaccio@kde.org>
 * SPDX-FileCopyrightText: 2019 David Barchiesi <david@barchie.si>
 * SPDX-FileCopyrightText: 2024 Bernardo Gomes Negri <b.gomes.negri@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 */


import URLHandler;


#include <tuple>
#include <QTest>

class UrlTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testOneDriveUrl() {
        QUrl specialUri = QUrl::fromEncoded("onedrive:/accountName/Documents");
        auto apiInfo = URLHandler().specialUriToApi(specialUri);
        QUrl urlResult = std::get<1>(apiInfo);
        QString accName = std::get<0>(apiInfo);
        QCOMPARE(accName, QStringLiteral("accountName"));
        QCOMPARE(urlResult.host(), QStringLiteral("graph.microsoft.com"));
        QCOMPARE(urlResult.scheme(), QStringLiteral("https"));
        QCOMPARE(urlResult.path(QUrl::FullyEncoded), QStringLiteral(""));
    }
};

QTEST_GUILESS_MAIN(UrlTest)
#include "urltest.moc"
